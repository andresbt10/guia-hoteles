$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    })

    $('#staticBackdrop').on("show.bs.modal", function(e){
        console.log("el modal se esta mostrando");
        $('#contactoBtn').removeClass("btn-outline-success");
        $('#contactoBtn').addClass("btn-primary");
        $('#contactoBtn').prop("disabled", true);
    });

    $('#staticBackdrop').on("shown.bs.modal", function(e){
        console.log("el modal se mostro");
    });

    $('#staticBackdrop').on("hide.bs.modal", function(e){
        console.log("el modal se esta ocultando");
        $('#contactoBtn').prop("disabled", false);
    });

    $('#staticBackdrop').on("hidden.bs.modal", function(e){
        console.log("el modal se oculto");
    });
})